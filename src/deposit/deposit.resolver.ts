/* eslint-disable prettier/prettier */
// src/resolvers/deposit.resolver.ts

import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { DepositService } from './deposit.service';
import { Deposit } from './deposit.entity';
import { UserService } from '../user/user.service';
import { TermService } from '../term/term.service';

@Resolver('Deposit')
export class DepositResolver {
  constructor(
    private readonly depositService: DepositService,
    private readonly userService: UserService,
    private readonly termService: TermService,
  ) {}

  @Query()
  async deposit(@Args('id') id: number): Promise<Deposit> {
    return this.depositService.getDepositById(id);
  }

  @Mutation()
  async createDeposit(
    @Args('userId') userId: number,
    @Args('termId') termId: number,
    @Args('amount') amount: number,
  ): Promise<Deposit> {
    const user = await this.userService.getUserById(userId);
    const term = await this.termService.getTermById(termId);
    console.log(user, term);
    if (!user || !term) {
      throw new Error('User or term not found');
    }
    const newDeposit = await this.depositService.createDeposit(
      user,
      term,
      amount,
    );
    return newDeposit;
  }

  @Mutation()
  async withdrawDeposit(@Args('id') id: number): Promise<number> {
    return this.depositService.withdrawDeposit(id);
  }
}
