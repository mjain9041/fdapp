// src/modules/deposit/deposit.module.ts

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Deposit } from './deposit.entity';
import { DepositResolver } from './deposit.resolver';
import { DepositService } from './deposit.service';
import { UserService } from '../user/user.service'; // Import UserService here
import { TermService } from '../term/term.service';
import { User } from '../user/user.entity';
import { Term } from '../term/term.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Deposit, User, Term])],
  providers: [DepositResolver, DepositService, UserService, TermService],
  exports: [DepositService], // Exporting DepositService to be used in other modules if needed
})
export class DepositModule {}
