// src/entities/deposit.entity.ts

import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
} from 'typeorm';
import { User } from '../user/user.entity';
import { Term } from '../term/term.entity';

@Entity()
export class Deposit {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Term, (term) => term.deposits)
  term: Term;

  @ManyToOne(() => User, (user) => user.deposits)
  user: User;

  @Column()
  amount: number;

  @Column({ default: false })
  isWithdrawn: boolean;

  @Column({ default: false })
  isInterestPaid: boolean;

  @CreateDateColumn()
  createdAt: Date;
}
