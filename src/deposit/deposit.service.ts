/* eslint-disable prettier/prettier */
// src/services/deposit.service.ts

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Deposit } from './deposit.entity';
import { User } from '../user/user.entity';
import { Term } from '../term/term.entity';

@Injectable()
export class DepositService {
  constructor(
    @InjectRepository(Deposit)
    private readonly depositRepository: Repository<Deposit>,
  ) {}

  async createDeposit(
    user: User,
    term: Term,
    amount: number,
  ): Promise<Deposit> {
    const deposit = new Deposit();
    deposit.user = user;
    deposit.term = term;
    deposit.amount = amount;
    return await this.depositRepository.save(deposit);
  }

  async getDepositById(id: any): Promise<Deposit> {
    return await this.depositRepository.findOne(id);
  }

  async updateDeposit(deposit: Deposit): Promise<Deposit> {
    return await this.depositRepository.save(deposit);
  }

  async withdrawDeposit(id: number): Promise<number> {
    const deposit = await this.depositRepository.findOne({
      where: { id },
      relations: ['term'],
    });

    if (!deposit) {
      throw new Error('Deposit not found');
    }

    const currentDate = new Date();
    const maturityDate = new Date(deposit.createdAt);
    maturityDate.setDate(maturityDate.getDate() + deposit.term.durationDays);
    if (currentDate < maturityDate) {
      throw new Error('Cannot withdraw before maturity');
    }

    const daysDiff = Math.ceil(
      (currentDate.getTime() - deposit.createdAt.getTime()) /
        (1000 * 60 * 60 * 24),
    );
    const interest =
      (deposit.amount * deposit.term.interestRate * daysDiff) / 36500;
    const totalAmount = deposit.amount + interest;

    deposit.isWithdrawn = true;
    await this.depositRepository.save(deposit);

    return totalAmount;
  }
}
