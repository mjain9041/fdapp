/* eslint-disable prettier/prettier */
// src/seeds/term.seed.ts

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Term } from '../term/term.entity';

@Injectable()
export class TermSeeder {
  constructor(
    @InjectRepository(Term)
    private readonly termRepository: Repository<Term>,
  ) {}

  async seed(): Promise<void> {
    const term7Exists = await this.termRepository.findOne({ where: { durationDays: 7 } });
    if (!term7Exists) {
      const term = new Term();
      term.durationDays = 7;
      term.interestRate = 10;
      await this.termRepository.save(term);
      console.log('Term with duration 7 days seeded successfully.');
    } else {
      console.log('Term with duration 7 days already exists.');
    }

    const term15Exists = await this.termRepository.findOne({ where: { durationDays: 15 } });
    if (!term15Exists) {
      const term = new Term();
      term.durationDays = 15;
      term.interestRate = 25;
      await this.termRepository.save(term);
      console.log('Term with duration 7 days seeded successfully.');
    } else {
      console.log('Term with duration 7 days already exists.');
    }
  }
}
