/* eslint-disable prettier/prettier */
// term.seed.service.ts

import { Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { TermSeeder } from './term.seed';

@Injectable()
export class TermSeedService implements OnApplicationBootstrap {
  constructor(private readonly termSeeder: TermSeeder) {}

  async onApplicationBootstrap(): Promise<void> {
    await this.termSeeder.seed();
    console.log('Term seed completed.');
  }
}
