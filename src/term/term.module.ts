/* eslint-disable prettier/prettier */
// src/modules/term/term.module.ts

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Term } from './term.entity';
import { TermService } from './term.service';
import { TermSeeder } from '../seeds/term.seed';
import { TermSeedService } from '../seeds/term.seed.service';

@Module({
  imports: [TypeOrmModule.forFeature([Term])],
  providers: [TermService, TermSeedService, TermSeeder],
  exports: [TermService], // Exporting TermService to be used in other modules if needed
})
export class TermModule {}
