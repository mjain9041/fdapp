// src/entities/term.entity.ts

import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Deposit } from '../deposit/deposit.entity';

@Entity()
export class Term {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  durationDays: number;

  @Column()
  interestRate: number;

  @OneToMany(() => Deposit, (deposit) => deposit.term)
  deposits: Deposit[];
}
