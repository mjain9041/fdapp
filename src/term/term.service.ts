// src/services/term.service.ts

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Term } from './term.entity';

@Injectable()
export class TermService {
  constructor(
    @InjectRepository(Term)
    private readonly termRepository: Repository<Term>,
  ) {}

  async getTermById(id: any): Promise<Term> {
    return await this.termRepository.findOne({ where: { id } });
  }

  async getAllTerms(): Promise<Term[]> {
    return await this.termRepository.find();
  }
}
